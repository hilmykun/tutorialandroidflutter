import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: PageMainAnimation(),debugShowCheckedModeBanner: false,
    );
  }
}

class PageMainAnimation extends StatelessWidget {
  const PageMainAnimation({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hero Animation'),
        backgroundColor: Colors.green,
      ),

      body: GestureDetector(
        child: Hero(
          tag: 'imageHero',
          child: Image.network('https://i.pinimg.com/originals/73/ba/1b/73ba1b297f74c570cd8aacadde02be92.jpg')),
          onTap:(){
            Navigator.push(context, MaterialPageRoute(builder: (_){
              return DetailHeroAnimation();
            }));
          },
      ),
    );
  }
}

class DetailHeroAnimation extends StatelessWidget {
  const DetailHeroAnimation({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(   
      child: Hero(tag: 'imageHero', child: Image.network('https://i.pinimg.com/originals/73/ba/1b/73ba1b297f74c570cd8aacadde02be92.jpg')),
      onTap: (){
        Navigator.pop(context);
      },
      ),
    );
  }
}